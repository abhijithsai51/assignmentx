package DLL;

public class Node {
    protected int data;
    protected Node next, prev;
 
    public Node() {
        next = getLinkNext();
        prev = getLinkPrev();
        data = 0;
    }
 
    public Node(int data, Node node, Node prev) {
        this.data = data;
        this.next = next;
        this.prev = prev;
    }
 
    public void setLinkNext(Node next) {
        this.next = next;
    }
 
    public void setLinkPrev(Node prev) {
        this.prev = prev;
    }
 
    public Node getLinkNext() {
        return next;
    }
 
    public Node getLinkPrev() {
        return prev;
    }
 
    public void setData(int d) {
        data = d;
    }
 
    public int getData() {
        return data;
    }
}


