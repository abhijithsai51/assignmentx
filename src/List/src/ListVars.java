package List.src;


import DLL.LinkedList;

public class ListVars
{
	Record current  = null;		// Pointer to current record to process in Linked List
    Record dllBegin = null; 	// Pointer to beginning record of Linked List
    Record dllEnd   = null;		// Pointer to last record of Linked List
    int	dllCount = 0; 		    // Number of records in the Linked List
    
    int	transCt   = 0;			// Number of transactions in queue to be processed
    int queAddNx  = 0;			// Next index position in queue to insert into next
	int queProcNx = 0;			// Next index position in queue to process 

}